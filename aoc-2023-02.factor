USING: multiline peg.ebnf set assocs.extra io.encodings.ascii ;

EBNF: game [=[
number     = ([0-9])+                    => [[ string>number ]]
game-id    = "Game " number ": "         => [[ second ]]
color      = "red" | "blue" | "green"
selection  = number " " color            => [[ first3 nip swap { } 2sequence ]]
selections =   selections ", " selection => [[ first3 nip suffix ]]
             | selection                 => [[ { } swap suffix ]]
handfuls   =   handfuls "; " selections  => [[ first3 nip fill-gaps suffix ]]
             | selections                => [[ fill-gaps { } swap suffix ]]
line       = game-id handfuls
]=]

: rgb ( r g b -- assoc )
  { } 3sequence { "red" "green" "blue" } swap zip ;

: fill-gaps ( handful -- handful )
  0 0 0 rgb swap assoc-union ;

: handfuls-to-max ( seq-of-assocs -- assoc )
  { } fill-gaps [ [ max ] assoc-merge ] reduce ;

: valid-handfuls? ( handfuls -- ? )
  handfuls-to-max
  12 13 14 rgb [ <= ] assoc-merge
  values t [ and ] reduce ;

: valid-game? ( game -- ? )
  second valid-handfuls? ;

: sum-valid-games ( games -- int )
  [ valid-game? ] filter
  [ first ] map sum ;

: lines-to-sum ( lines -- int )
  [ game ] map sum-valid-games ;

"aoc2023-2.txt" ascii [ read-lines lines-to-sum ] with-file-reader


: sum-powers ( games -- int )
  [ second handfuls-to-max values product ] map sum ;

: lines-to-power-sum ( lines -- int )
  [ game ] map sum-powers ;

"aoc2023-2.txt" ascii [ read-lines lines-to-power-sum ] with-file-reader
