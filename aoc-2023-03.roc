app "hello"
    packages { pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br" }
    imports [
        pf.Stdout,
        "./inputs/aoc-2023-03.txt" as input : Str,
        "./sample-inputs/aoc-2023-03.txt" as sample : Str ]
    provides [main] to pf

parse = \input ->
    clean = input |> Str.trim |> Str.split "\n"
    rowaccum, row, rowindex <- List.walkWithIndex clean []
    List.append rowaccum
        accum, el, colindex <- List.walkWithIndex row []
        when el is
          '.' -> accum
          b if '0' <= b && b <= '9' -> List.append accum (Digit { row: rowindex, col: colindex, chr: b })
          _ -> List.append accum (Symbol { row: rowindex, col: colindex })

expect
    grid = ".*.\n3.4\n5$."
    parse grid
    == [
        [ Symbol { row: 0, col: 1 } ],
        [ Digit { row: 1, col: 0, chr: '3' },
          Digit { row: 1, col: 2, chr: '4' }],
        [ Digit { row: 2, col: 0, chr: '5'},
          Symbol { row: 2, col: 1 } ]
    ]

main =
    Stdout.line "I'm a Roc application!"