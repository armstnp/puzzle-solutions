(ns advent-of-code-2023.day4
  (:require [advent-of-code-2023.core :as core]
            [clojure.string :as str]))

(def input
  (->> "aoc-2023-04.txt" core/read-input str/split-lines))

(def sample-input
  (->> "aoc-2023-04-sample.txt" core/read-input str/split-lines))

(defn line->card [line]
  (-> line (str/split #":") second (str/split #"\|")
      (->> (map #(map core/parse-int (re-seq #"\d+" %))))))
(clojure.test/is (= (line->card (first sample-input)) '((41 48 83 86 17) (83 86 6 31 17 9 48 53))))

(defn card->score [[winners numbers]]
  (let [winners (into #{} winners)
        num-matching (count (filter winners numbers))]
    (if (zero? num-matching) 0
        (bit-shift-left 1 (dec num-matching)))))
(clojure.test/is (= (map (comp card->score line->card) sample-input) '(8 2 2 1 0 0)))

(defn cards->score [cards]
  (reduce + (map card->score cards)))

(defn solve-p1 [lines]
  (cards->score (map line->card lines)))
(clojure.test/is (= (solve-p1 sample-input) 13))

(defn roll [cards]
  (reduce 
   (fn [accum [winners numbers]]
     (let [num-matching (count (filter (into #{} winners) numbers))
           downstream (reduce + (take num-matching accum))]
       (cons (inc downstream) accum)))
   '()
   (reverse cards)))
(clojure.test/is (= (roll (map line->card sample-input)) '(15 7 4 2 1 1)))

(defn solve-p2 [lines]
  (->> lines (map line->card) roll (reduce +)))
(clojure.test/is (= (solve-p2 sample-input) 30))