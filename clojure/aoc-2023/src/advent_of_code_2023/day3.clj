(ns advent-of-code-2023.day3
  (:require [advent-of-code-2023.core :as core]
            [clojure.string :as str]))

(defn describe [input]
  (let [width (count (first input))
        coord->cell (fn [{:keys [row col] :as m}]
                      (assoc m :cell (+ col (* row width))))]
    {:height      (count input)
     :width       width
     :coord->cell coord->cell
     :rows        input}))

(def input
  (->> "aoc-2023-03.txt" core/read-input str/split-lines describe))

(def sample-input
  (->> "aoc-2023-03-sample.txt" core/read-input str/split-lines describe))

(defn input->symbols [{:keys [rows coord->cell] :as input}]
  (let [sym? (complement (into #{} (seq ".0123456789")))
        coords (apply concat
                      (map-indexed (fn [y row]
                                     (map-indexed (fn [x el]
                                                    (coord->cell {:row y :col x :val el}))
                                                  row))
                                   rows))
        syms (filter (comp sym? :val) coords)]
    (assoc input :symbols syms)))

(core/defn-split ring [{:keys [width height coord->cell]} | {:keys [row col]}]
  (for [y [(dec row) row (inc row)]
        :when (and (>= y 0) (< y height))
        x [(dec col) col (inc col)]
        :when (and (>= x 0) (< x width) (or (not= y row) (not= x col)))]
    (coord->cell {:row y :col x})))

(defn symbol->bitring [input]
  (let [ring (ring input)]
    (fn [symbol]
      (let [cells (->> symbol ring (map :cell) (into #{}))]
        (-> symbol
            (assoc :ring cells)
            (assoc :actives (fn [{:keys [cell]}] (cells cell))))))))

(defn symbols->bitring [input]
  (let [symbol->bitring (symbol->bitring input)]
    (update input :symbols #(map symbol->bitring %))))

(defn symbols->actives [{:keys [symbols] :as input}]
  (let [actives (->> symbols (map :ring) (reduce into))]
    (assoc input :actives (fn [{:keys [cell]}] (actives cell)))))

(def input->actives (comp #'symbols->actives #'symbols->bitring #'input->symbols))

(core/defn-split re-match-results [re | s]
  (-> (re-matcher re s) .results .iterator iterator-seq))

(defn input->numbers [{:keys [rows coord->cell] :as input}]
  (let [digit-matches (re-match-results #"\d+")]
    (letfn [(row->numbers [y row]
              (map (fn [m]
                     {:val (core/parse-int (.group m))
                      :coords (for [x (range (.start m) (.end m))]
                                (coord->cell {:row y :col x}))})
                   (digit-matches row)))]
      (assoc input :numbers (apply concat (map-indexed row->numbers rows))))))

(def input->blackboard (comp #'input->numbers #'input->actives))

;; Now for destructive action

(defn filter-active-numbers [{:keys [actives] :as input}]
  (update input :numbers #(filter (fn [{:keys [coords]}] (some actives coords)) %)))

(defn sum-numbers [{:keys [numbers]}]
  (->> numbers (map :val) (reduce + 0)))

(def solve-p1 (comp #'sum-numbers #'filter-active-numbers #'input->blackboard))

(clojure.test/is (= (#'solve-p1 sample-input) 4361))

(defn blackboard->gear-ratios [{:keys [numbers symbols] :as input}]
  (let [potential-gears (filter (comp #(= % \*) :val) symbols)
        adj-nums (fn [{:keys [actives]}]
                   (filter (fn [{:keys [coords]}] (some actives coords)) numbers))]
    (->> potential-gears
         (map adj-nums)
         (filter #(= 2 (count %)))
         (map (fn [[x y]] (* (:val x) (:val y))))
         (reduce +))))

(def solve-p2 (comp #'blackboard->gear-ratios #'filter-active-numbers #'input->blackboard))

(clojure.test/is (= (#'solve-p2 sample-input) 467835))