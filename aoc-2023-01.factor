USING: regexp math.parser io.encodings.ascii ;

MEMO: word-digits ( -- assoc )
  { { "one"   1 }
    { "1"     1 }
    { "two"   2 }
    { "2"     2 }
    { "three" 3 }
    { "3"     3 }
    { "four"  4 }
    { "4"     4 }
    { "five"  5 }
    { "5"     5 }
    { "six"   6 }
    { "6"     6 }
    { "seven" 7 }
    { "7"     7 }
    { "eight" 8 }
    { "8"     8 }
    { "nine"  9 }
    { "9"     9 }
    { "zero"  0 }
    { "0"     0 } } ;

MEMO: re-text ( -- string )
  word-digits keys "|" join ;

MEMO: first-regexp ( -- regexp )
  re-text <regexp> ;

MEMO: last-regexp ( -- regexp )
  re-text "r" <optioned-regexp> ;

: parse-digit ( slice -- int )
  >string word-digits at ;

: first-digit ( line -- int )
  first-regexp first-match parse-digit ;

: last-digit ( line -- int )
  last-regexp first-match parse-digit ;

: line-to-digits ( line -- int )
  dup first-digit 10 * swap last-digit + ;

: lines-to-sum ( lineseq -- )
  [ line-to-digits ] map sum >dec print ;

: file-to-digits ( filename -- )
  ascii [ read-lines lines-to-sum ] with-file-reader ;

"aoc2023-1.txt" file-to-digits